from nnTorch import *
from nncfTorch import *

class NeuralNetworkClassifierConvolutionalTorch(NeuralNetworkClassifierTorch):

    def __init__(self, n_inputs, n_conv_list, n_fc_list, n_outputs, device='cpu'):
        '''n_inputs: n_channels X n_rows X n_cols
n_conv_list: list of tuples of (n_units, kernel_size, stride)
n_fc_list: list of integers for number of units in each fully connected layer'''

        # Call constructor, but will discard the layers made and make new ones
        super().__init__(n_inputs, n_fc_list, n_outputs, device)

        self.n_channels, self.n_input_rows, self.n_input_cols = n_inputs
        self.n_conv_list = n_conv_list
        self.n_fc_list = n_fc_list
        self.n_outputs = n_outputs
        self.device = device

        self.layers = torch.nn.ModuleList()
        n_in_channels = self.n_channels
        
        # Initially o/p rows, cols and channels from image
        output_rows, output_cols, output_channels = [self.n_input_rows, self.n_input_cols, self.n_channels]
        #o/p_channlles = num of units


        # [1, 20, 20]
        # [(3, (3,3), (2,2)), (4, (5,5), (2,2))]
        # [5,5]
        # o/p = [(20 - 3) // stride] +1 = 17//2 +1  = 9 = 9x9 
        # o/p col = (n ip cols - kernel cols)// stride rows +1 
        # o/p row = ..?
        # o/p channel = 

        for conv in n_conv_list:

            n_units, kernel_size, stride = conv
            (kernel_rows, kernel_cols) = (kernel_size, kernel_size) if np.isscalar(kernel_size) else kernel_size
            (stride_rows, stride_cols) = (stride, stride) if np.isscalar(stride) else stride

            self.layers.append(self._make_conv2d_layer(n_in_channels, n_units, kernel_size, stride))
            n_in_channels = n_units

            output_rows, output_cols, output_channels = self._get_output_row_col_channel(
                n_units,
                output_rows,
                output_cols,
                kernel_rows,
                kernel_cols,
                stride_rows,
                stride_cols)  # COMPLETE THIS

            #self.layers.append(self._make_conv2d_layer(n_in_channels, n_units, kernel_size, stride))
            #n_in_channels = n_units
            

            print('conv output rows {}, cols {}, channels {}:'.format(output_rows, output_cols, output_channels))

        # Add Flatten to output of last convolutional layer
        self.layers[-1].add_module('flatten', torch.nn.Flatten())

        # Calculate the total number of inputs entering each unit in the first fully-connected layer

        # number of pixels in the last layer of output from conv layer
        # 
        n_inputs = output_rows * output_cols * output_channels   # COMPLETE THIS

        for n_units in n_fc_list:
            self.layers.append(self._make_tanh_layer(n_inputs, n_units))
            n_inputs = n_units

        self.layers.append(torch.nn.Sequential(torch.nn.Linear(n_inputs, n_outputs),
                                               torch.nn.LogSoftmax(dim=1)))  # across columns

    def _make_conv2d_layer(self, n_in_channels, n_units, kernel_size, stride):
        return torch.nn.Sequential(
                   torch.nn.Conv2d(in_channels=n_in_channels,
                                   out_channels=n_units,
                                   kernel_size=kernel_size,
                                   stride=stride),
                   torch.nn.Tanh() ) # COMPLETE THIS as Conv2d followed by Tanh 

    def _get_output_row_col_channel(self,
                                    n_units,
                                    output_rows,
                                    output_cols,
                                    kernel_rows,
                                    kernel_cols,
                                    stride_rows,
                                    stride_cols):

        out_rows = (output_rows - kernel_rows) // stride_rows + 1
        out_cols = (output_cols - kernel_cols) // stride_cols + 1
        out_channels = n_units

        return (out_rows, out_cols, out_channels)


    def __repr__(self):
        return f'''NeuralNetworkClassifierConvolutionalTorch(n_inputs={self.n_inputs}, n_conv_list={self.n_conv_list}, n_fc_list={self.n_fc_list}, n_outputs={self.n_outputs}, device={self.device})'''